package com.moviereviews.base

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import com.moviereviews.R

open class BaseActivity:AppCompatActivity() {

    private var alertDialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

    }


    fun showProgressBar() {

        if (alertDialog != null && alertDialog!!.isShowing && this.isFinishing) {
            return
        }
        alertDialog = Dialog(this)
        alertDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog!!.setContentView(R.layout.progress_dialog)
        alertDialog!!.setCancelable(true)
        alertDialog!!.show()

    }

    fun hideProgressBar() {

        if (alertDialog != null && alertDialog!!.isShowing) {
            alertDialog!!.dismiss()
            alertDialog = null
        }
    }
}