package com.moviereviews.model

data class Error(var fault:Fault)
data class Fault(var faultstring:String, var detail:Detail)
data class Detail(var errorcode:String)
