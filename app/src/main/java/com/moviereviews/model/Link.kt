package com.moviereviews.model

import java.io.Serializable

data class Link(var type:String,var url:String,var suggested_link_text:String):Serializable
