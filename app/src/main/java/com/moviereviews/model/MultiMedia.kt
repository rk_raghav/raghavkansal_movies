package com.moviereviews.model

import java.io.Serializable

data class MultiMedia(var type:String,var src:String,var height:Int,var width:Int):Serializable
