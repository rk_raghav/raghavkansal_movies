package com.moviereviews.model

data class Reviews(var status:String,var copyright:String,var has_more:Boolean,var num_results:Int,var results:List<Result>)
