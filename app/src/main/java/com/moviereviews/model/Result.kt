package com.moviereviews.model

import java.io.Serializable

data class Result(var display_title:String,var mpaa_rating:String,var critics_pick:Int,var byline:String,
                  var headline:String,var summary_short:String,var publication_date:String,var opening_date:String,
                  var date_updated:String,var link:Link,var multimedia:MultiMedia):Serializable
