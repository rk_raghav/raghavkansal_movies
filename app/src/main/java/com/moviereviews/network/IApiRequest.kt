package com.moviereviews.network

import com.moviereviews.model.Reviews
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface IApiRequest {

    @GET("reviews/search.json")
    suspend fun getMoviesResponses(@Query("api-key") api_key:String) : Response<Reviews>
}