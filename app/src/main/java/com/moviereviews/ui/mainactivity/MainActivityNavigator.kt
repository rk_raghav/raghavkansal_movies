package com.moviereviews.ui.mainactivity

import com.moviereviews.model.Result

interface MainActivityNavigator {
    fun clickMovie(result: Result)

}