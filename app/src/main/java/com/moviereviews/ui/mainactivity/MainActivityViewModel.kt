package com.moviereviews.ui.mainactivity

import androidx.lifecycle.MutableLiveData
import com.moviereviews.BuildConfig
import com.moviereviews.base.BaseViewModel
import com.moviereviews.model.Error
import com.moviereviews.model.Reviews
import com.moviereviews.network.RetrofitClient
import com.moviereviews.utils.GsonUtils
import kotlinx.coroutines.*
import retrofit2.Response

class MainActivityViewModel:BaseViewModel<MainActivityNavigator, MainActivityRepo>() {

    var movieResponseSuccess = MutableLiveData<Reviews>()
    var movieResponseError = MutableLiveData<Error>()

    private var job: Job?=null

    fun getMovies(){

        CoroutineScope(Dispatchers.IO).launch {

            val response:Response<Reviews> = RetrofitClient.request?.getMoviesResponses(BuildConfig.key)!!

            withContext(Dispatchers.Main){

                if (response.isSuccessful){
                  movieResponseSuccess.postValue(response.body())
                }

                else {
                    var error: Error? = null

                    try {
                        error = GsonUtils.parseJson(String(response.errorBody()!!.bytes()), Error::class.java)
                        movieResponseError.postValue(error)
                    }

                    catch (e: Exception) {
                        movieResponseError.postValue(error)
                    }
                }

            }
        }

    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }

}