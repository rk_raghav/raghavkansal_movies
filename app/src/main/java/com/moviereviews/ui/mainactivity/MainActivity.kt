package com.moviereviews.ui.mainactivity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.moviereviews.R
import com.moviereviews.adapter.MovieAdapter
import com.moviereviews.base.BaseActivity
import com.moviereviews.databinding.ActivityMainLayoutBinding
import com.moviereviews.model.Result
import com.moviereviews.network.RetrofitClient
import com.moviereviews.ui.detailactivity.ActivityDetail
import com.moviereviews.utils.AppConstant

class MainActivity : BaseActivity() , MainActivityNavigator {

    private lateinit var binding: ActivityMainLayoutBinding
    private lateinit var viewModel: MainActivityViewModel

    private lateinit var adapter:MovieAdapter
    private var listMovies:MutableList<Result> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RetrofitClient.init()
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main_layout)
        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)
        viewModel.setNavigator(this)
        val repo = MainActivityRepo()
        viewModel.setRepo(repo)
        initAdapter()
        observer()

        binding.model = viewModel

    }

    private fun initAdapter(){
        adapter = MovieAdapter(this, listMovies,this)
        binding.adapter = adapter
    }

    private fun observer(){

        viewModel.movieResponseSuccess.observe(this, {

            hideProgressBar()
            listMovies.clear()
            listMovies.addAll(it.results)
            adapter.notifyDataSetChanged()

        })

        viewModel.movieResponseError.observe(this, {
            hideProgressBar()
            Toast.makeText(this,it.fault.faultstring,Toast.LENGTH_SHORT).show()
        })

    }

    override fun onResume() {
        super.onResume()
        showProgressBar()
        viewModel.getMovies()
    }

    override fun clickMovie(result: Result) {

        val intent = Intent(this, ActivityDetail::class.java)
        intent.putExtra(AppConstant.DETAILS,result)
        startActivity(intent)
    }
}