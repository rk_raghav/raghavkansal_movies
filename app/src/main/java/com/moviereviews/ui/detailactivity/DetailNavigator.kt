package com.moviereviews.ui.detailactivity

interface DetailNavigator {
    fun back()
}