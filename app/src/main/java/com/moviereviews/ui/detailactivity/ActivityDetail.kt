package com.moviereviews.ui.detailactivity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.moviereviews.R
import com.moviereviews.base.BaseActivity
import com.moviereviews.databinding.ActivityDetailBinding
import com.moviereviews.model.Result
import com.moviereviews.utils.AppConstant

class ActivityDetail:BaseActivity() ,DetailNavigator{

    private lateinit var binding:ActivityDetailBinding
    private lateinit var model: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        model = ViewModelProvider(this).get(DetailViewModel::class.java)
        model.setNavigator(this)
        val repo = DetailRepo()
        model.setRepo(repo)

        val result = intent.getSerializableExtra(AppConstant.DETAILS) as Result
        binding.response  = result

        binding.click = this
        binding.detailModel = model

    }

    override fun back() {
        finish()
    }
}