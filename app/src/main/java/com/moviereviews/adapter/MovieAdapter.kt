package com.moviereviews.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.moviereviews.BR

import com.moviereviews.R
import com.moviereviews.databinding.ItemMovieBinding
import com.moviereviews.model.Result
import com.moviereviews.ui.mainactivity.MainActivityNavigator

class MovieAdapter(private var context:Context, private var movieList: List<Result>,private var click: MainActivityNavigator): RecyclerView.Adapter<MovieAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemMovieBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_movie, parent, false)
        return ViewHolder(binding,click)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(movieList[position])

    }

    override fun getItemCount(): Int {
        return movieList.size
    }


    class ViewHolder(private var view:ItemMovieBinding,private var click: MainActivityNavigator):RecyclerView.ViewHolder(view.root) {

        fun bind(response:Result){
            view.setVariable(BR.response,response)
            view.setVariable(BR.click,click)
            view.executePendingBindings()
        }
    }

}