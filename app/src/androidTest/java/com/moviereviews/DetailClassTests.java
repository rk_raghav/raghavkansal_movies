package com.moviereviews;

import androidx.test.ext.junit.rules.ActivityScenarioRule;

import com.moviereviews.ui.mainactivity.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class DetailClassTests {

    @Rule
    public ActivityScenarioRule<MainActivity> activityTestRule = new ActivityScenarioRule<>(MainActivity.class);

    /**
     * Test Recyclerview Displayed or not
     */
    @Test
    public void testRvDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).check(matches(isDisplayed()));
    }


    /**
     * Test Image Displayed or not
     */
    @Test
    public void testImageDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.user_img)).check(matches(isDisplayed()));
    }


    /**
     * Test Title Displayed or not
     */
    @Test
    public void testTitleDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.name)).check(matches(isDisplayed()));
    }

    /**
     * Test Rating Heading Displayed or not
     */
    @Test
    public void testRatingHeadDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_rating_head)).check(matches(isDisplayed()));
    }


    /**
     * Test Rating Value Displayed or not
     */
    @Test
    public void testRatingValDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_rating_value)).check(matches(isDisplayed()));
    }


    /**
     * Test Rating Head Name or not
     */
    @Test
    public void testRatingHeadName() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_rating_head)).check(matches(withText("Rating")));
    }






    /**
     * Test Byline Heading Displayed or not
     */
    @Test
    public void testBylineHeadDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_byline_head)).check(matches(isDisplayed()));
    }


    /**
     * Test Byline Value Displayed or not
     */
    @Test
    public void testBylineValDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_byline_val)).check(matches(isDisplayed()));
    }



    /**
     * Test Byline Head Name or not
     */
    @Test
    public void testBylineHeadName() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_byline_head)).check(matches(withText("Byline")));
    }



    /**
     * Test Headline Heading Displayed or not
     */
    @Test
    public void testHeadlineHeadDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_headline_head)).check(matches(isDisplayed()));
    }


    /**
     * Test Headline Value Displayed or not
     */
    @Test
    public void testHeadlineValDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_headline_value)).check(matches(isDisplayed()));
    }


    /**
     * Test Headline Head Name or not
     */
    @Test
    public void testHeadlineHeadName() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_headline_head)).check(matches(withText("Headline")));
    }



    /**
     * Test Summary Heading Displayed or not
     */
    @Test
    public void testSummaryHeadDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_summary_head)).check(matches(isDisplayed()));
    }


    /**
     * Test Summary Value Displayed or not
     */
    @Test
    public void testSummaryValDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_summary_value)).check(matches(isDisplayed()));
    }


    /**
     * Test Summary Head Name or not
     */
    @Test
    public void testSummaryHeadName() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_summary_head)).check(matches(withText("Summary")));
    }

    /**
     * Test PublishDate Heading Displayed or not
     */
    @Test
    public void testPublishDateHeadDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_date)).check(matches(isDisplayed()));
    }


    /**
     * Test PublishDate Value Displayed or not
     */
    @Test
    public void testPublishDateValDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_date_publish)).check(matches(isDisplayed()));
    }


    /**
     * Test PublishDate Head Name or not
     */
    @Test
    public void testPublishDateHeadName() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_date)).check(matches(withText("Publication Date")));
    }

    /**
     * Test OpenDate Heading Displayed or not
     */
    @Test
    public void testOpenDateHeadDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_opening)).check(matches(isDisplayed()));
    }


    /**
     * Test OpenDate Value Displayed or not
     */
    @Test
    public void testOpenDateValDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_date_open)).check(matches(isDisplayed()));
    }


    /**
     * Test OpenDate Head Name or not
     */
    @Test
    public void testOpenDateHeadName() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_opening)).check(matches(withText("Opening Date")));
    }

    /**
     * Test URL Heading Displayed or not
     */
    @Test
    public void testUrlHeadDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_url)).check(matches(isDisplayed()));
    }


    /**
     * Test URL Value Displayed or not
     */
    @Test
    public void testUrlValDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_url_detail)).check(matches(isDisplayed()));
    }


    /**
     * Test URL Head Name or not
     */
    @Test
    public void testURLHeadName() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.txt_url)).check(matches(withText("URL")));
    }


    /**
     * Test Back Button Displayed or not
     */
    @Test
    public void testBackDisplayed() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.ivBack)).check(matches(isDisplayed()));
    }


    /**
     * Test Back Button Working or not
     */
    @Test
    public void testBackWorking() throws InterruptedException {
        Thread.sleep(3000);
        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition(0, click()));
        Thread.sleep(1000);
        onView(withId(R.id.ivBack)).check(matches(isDisplayed())).perform(click());
        Thread.sleep(1000);
        onView(withId(R.id.rvMovies)).check(matches(isDisplayed()));

    }
}
